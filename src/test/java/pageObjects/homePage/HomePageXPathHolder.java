package pageObjects.homePage;

class HomePageXPathHolder {
    public static final String FIRST_CHAT_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View[1]";
}
