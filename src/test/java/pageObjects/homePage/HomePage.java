package pageObjects.homePage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageObjects.PageObject;
import pageObjects.chatPage.ChatPage;

public class HomePage extends PageObject {

    public final By firstChatLocator = By.xpath(HomePageXPathHolder.FIRST_CHAT_XPATH);

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public ChatPage clickFirstChat() {
        driver.findElement(firstChatLocator).click();
        return new ChatPage(driver);
    }
}
