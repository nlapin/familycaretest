package pageObjects.chatPage;

class ChatPageXPathHolder {
    public static final String MESSAGE_BOX_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View[3]/android.view.View[2]/android.view.View[2]/android.view.View[1]/" +
            "android.widget.EditText";

    public static final String SEND_BUTTON_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View[3]/android.view.View[2]/android.view.View[2]/android.view.View[2]/" +
            "android.view.View[1]/android.widget.Button";

    public static final String CHAT_LIST_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View[2]";
}
