package pageObjects.chatPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.PageObject;

import java.util.List;

public class ChatPage extends PageObject {

    public final By messageBoxLocator = By.xpath(ChatPageXPathHolder.MESSAGE_BOX_XPATH);
    public final By sendButtonLocator = By.xpath(ChatPageXPathHolder.SEND_BUTTON_XPATH);
    public final By chatListLocator = By.xpath(ChatPageXPathHolder.CHAT_LIST_XPATH);

    public ChatPage(WebDriver driver) {
        super(driver);
    }

    public ChatPage typeMessage(String message) {
        driver.findElement(messageBoxLocator).sendKeys(message);
        return this;
    }

    public ChatPage clickSendButton() {
        driver.findElement(sendButtonLocator).click();
        return this;
    }

    public ChatPage sendMessage(String message) {
        typeMessage(message);
        return clickSendButton();
    }

    public boolean isLastMessageMatch(String message) {
        List<WebElement> elements = driver.findElement(chatListLocator).findElements(By.xpath(".//*"));
        // Id of the last message is [elements.size() - 2]
        WebElement webElement = elements.get(elements.size() - 2);
        String text = webElement.getText();
        return message.equals(text);
    }
}
