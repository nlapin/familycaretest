package pageObjects.batteryOptimizationPage;

class BatteryOptimizationPageXPathHolder {
    public static final String SKIP_BUTTON_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View[3]/android.view.View[5]/android.widget.Button";
}
