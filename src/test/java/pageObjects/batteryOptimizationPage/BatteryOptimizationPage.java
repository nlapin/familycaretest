package pageObjects.batteryOptimizationPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageObjects.PageObject;
import pageObjects.homePage.HomePage;

public class BatteryOptimizationPage extends PageObject {

    public final By skipButtonLocator = By.xpath(BatteryOptimizationPageXPathHolder.SKIP_BUTTON_XPATH);

    public BatteryOptimizationPage(WebDriver driver) {
        super(driver);
    }

    public HomePage clickSkipButton() {
        driver.findElement(skipButtonLocator).click();
        return new HomePage(driver);
    }
}
