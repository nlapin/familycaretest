package pageObjects.loginPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageObjects.PageObject;
import pageObjects.batteryOptimizationPage.BatteryOptimizationPage;

public class LoginPage extends PageObject {

    public final By emailLocator = By.xpath(LoginPageXPathHolder.EMAIL_XPATH);
    public final By passwordLocator = By.xpath(LoginPageXPathHolder.PASSWORD_XPATH);
    public final By loginButtonLocator = By.xpath(LoginPageXPathHolder.LOGIN_BUTTON_XPATH);

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage typeEmail(String email) {
        driver.findElement(emailLocator).sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password) {
        driver.findElement(passwordLocator).sendKeys(password);
        return this;
    }

    public BatteryOptimizationPage submitLogin() {
        driver.findElement(loginButtonLocator).click();
        return new BatteryOptimizationPage(driver);
    }

    public BatteryOptimizationPage loginAs(String username, String password) {
        typeEmail(username);
        typePassword(password);
        return submitLogin();
    }
}
