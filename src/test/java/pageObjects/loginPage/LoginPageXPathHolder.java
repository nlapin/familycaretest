package pageObjects.loginPage;

class LoginPageXPathHolder {
    public static final String EMAIL_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/" +
            "android.widget.EditText";

    public static final String PASSWORD_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[3]/" +
            "android.widget.EditText";

    public static final String LOGIN_BUTTON_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[4]/" +
            "android.widget.Button";
}
