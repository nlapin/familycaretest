package domain;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

public class ConfigurationParser {
    private String deviceName;
    private String platformVersion;
    private String platformName;
    private String pathToApk;
    private String serverIp;
    private String serverPort;
    private String email;
    private String password;
    private JSONObject jsonObj;

    public ConfigurationParser() throws IOException, ParseException {
        URL resource = Thread.currentThread().getContextClassLoader().getResource("configuration.json");
        parseConfigFile(resource.getPath());
    }

    private void parseConfigFile(String pathToFile) throws IOException, ParseException {
        FileReader fileReader = new FileReader(pathToFile);
        final JSONParser jsonParser = new JSONParser();
        jsonObj = (JSONObject) jsonParser.parse(fileReader);
        deviceName = (String) jsonObj.get("deviceName");
        platformName = (String) jsonObj.get("platformName");
        platformVersion = (String) jsonObj.get("platformVersion");
        pathToApk = (String) jsonObj.get("pathToApk");
        serverIp = (String) jsonObj.get("serverIp");
        serverPort = (String) jsonObj.get("serverPort");
        email = (String) jsonObj.get("email");
        password = (String) jsonObj.get("password");
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public String getPlatformName() {
        return platformName;
    }

    public String getPathToApk() {
        return pathToApk;
    }

    public String getServerIp() {
        return serverIp;
    }

    public String getServerPort() {
        return serverPort;
    }

    public Object getCustomParam(String key) {
        return jsonObj.get(key);
    }
}
