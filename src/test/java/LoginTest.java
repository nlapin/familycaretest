import domain.ConfigurationParser;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.core.annotation.Order;
import pageObjects.batteryOptimizationPage.BatteryOptimizationPage;
import pageObjects.chatPage.ChatPage;
import pageObjects.homePage.HomePage;
import pageObjects.loginPage.LoginPage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;

public class LoginTest {

    private ConfigurationParser configurationParser;
    private AndroidDriver<MobileElement> driver;

    @Before
    public void before() throws IOException, ParseException {
        configurationParser = new ConfigurationParser();
        String deviceName = configurationParser.getDeviceName();
        String pathToApk = configurationParser.getPathToApk();
        String platformName = configurationParser.getPlatformName();
        String platformVersion = configurationParser.getPlatformVersion();

        File app = new File(pathToApk);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
        capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
        capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, "true");
        URL remoteAddress = new URL("http://" + configurationParser.getServerIp() + ":"
                + configurationParser.getServerPort() + "/wd/hub");
        driver = new AndroidDriver<>(remoteAddress, capabilities);
    }

    @Test
    @Order(0)
    public void testLoginPage() throws InterruptedException {
        final LoginPage loginPage = new LoginPage(driver);
        waitForElementToBeClickable(loginPage.emailLocator);

        final BatteryOptimizationPage batteryOptimizationPage = loginPage.loginAs(
                configurationParser.getEmail(),
                configurationParser.getPassword());
        waitForElementToBeClickable(batteryOptimizationPage.skipButtonLocator);

        final HomePage homePage = batteryOptimizationPage.clickSkipButton();
        waitForElementToBeClickable(homePage.firstChatLocator);

        final ChatPage chatPage = homePage.clickFirstChat();
        waitForElementToBeClickable(chatPage.messageBoxLocator);

        String test_message = "Test message " + new Date().toString();
        chatPage.sendMessage(test_message);
        //Delay for server to fetch new messages
        Thread.sleep(3_000);
        assert chatPage.isLastMessageMatch(test_message);
    }

    private void waitForElementToBeClickable(By by) {
        WebDriverWait webDriver = new WebDriverWait(driver, 30);
        webDriver.until(ExpectedConditions.elementToBeClickable(by));
    }
}
