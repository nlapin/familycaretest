# Testing Media4Care

For testing, we have used **Appium** and **Selenium** instruments.

Appium provides several approaches to get access to the element:
http://appium.io/docs/en/commands/element/find-element/
```java
//First approach - by unique id of the element
MobileElement elementOne = (MobileElement) driver.findElementByAccessibilityId("SomeAccessibilityID");
//Second approach - by class element (not recommended to use as the xpath approach)
MobileElement elementTwo = (MobileElement) driver.findElementByClassName("SomeClassName");
```
---

To follow best practices we have decided to use first approach:
```java
MobileElement elementOne = (MobileElement) driver.findElementByAccessibilityId("SomeAccessibilityID");
```

But we have faced the issue that method `AndroidDriver::findElementByAccessibilityId` returns null as well as `By::id`

So, we are using **xpath** approach:
```java
public class LoginPage extends PageObject {

    public final By emailLocator = By.xpath(LoginPageXPathHolder.EMAIL_XPATH);
    public final By passwordLocator = By.xpath(LoginPageXPathHolder.PASSWORD_XPATH);
    public final By loginButtonLocator = By.xpath(LoginPageXPathHolder.LOGIN_BUTTON_XPATH);

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage typeEmail(String email) {
        driver.findElement(emailLocator).sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password) {
        driver.findElement(passwordLocator).sendKeys(password);
        return this;
    }

    public BatteryOptimizationPage submitLogin() {
        driver.findElement(loginButtonLocator).click();
        return new BatteryOptimizationPage(driver);
    }

    public BatteryOptimizationPage loginAs(String username, String password) {
        typeEmail(username);
        typePassword(password);
        return submitLogin();
    }
}


/**
* Class that holds all the required elements
*/
class LoginPageXPathHolder {
    public static final String EMAIL_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/" +
            "android.widget.EditText";

    public static final String PASSWORD_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[3]/" +
            "android.widget.EditText";

    public static final String LOGIN_BUTTON_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/" +
            "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
            "android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[4]/" +
            "android.widget.Button";
}
```